#include "cJSON.h"

#include "../test/pub.c"

int press_cJSON( char *pathfilename , int count )
{
	char		*json = NULL ;
	cJSON		*root = NULL ;
	int		i ;
	int		nret = 0 ;
	
	nret = ReadEntireFileSafely( pathfilename , "r" , & json , NULL ) ;
	if( nret )
	{
		printf( "*** ERROR : ParseJsonToDom failed\n" );
		return -1;
	}
	
	for( i = 0 ; i < count ; i++ )
	{
		root = cJSON_Parse( json ) ;
		if( root == NULL )
		{
			printf( "*** ERROR : ParseJsonToDom failed\n" );
			return -1;
		}
		
		cJSON_Delete( root );
	}
	
	free( json );
	
	return 0;
}

int main( int argc , char *argv[] )
{
	if( argc != 1 + 2 )
	{
		printf( "USAGE : press_cJSON json_file count\n" );
		exit(7);
	}
	
	return -press_cJSON( argv[1] , atoi(argv[2]));
}

