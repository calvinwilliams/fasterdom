#include "fasterdom4fasterxml.h"

#include "pub.h"

int test_fasterdom4fasterxml( char *pathfilename )
{
	char		*xml = NULL ;
	char		*xml2 = NULL ;
	struct DomNode	*root = NULL ;
	struct DomNode	*root2 = NULL ;
	int		nret = 0 ;
	
	nret = ReadEntireFileSafely( pathfilename , "rb" , & xml , NULL ) ;
	if( nret )
	{
		printf( "*** ERROR : ReadEntireFileSafely failed\n" );
		return -1;
	}
	printf( "Read file to xml[%s]\n" , xml );
	
	root = ParseXmlToDom( xml ) ;
	free( xml );
	if( root == NULL )
	{
		printf( "*** ERROR : ParseXmlToDom failed[%d]\n" , GetParserLastError() );
		return -1;
	}
	printf( "Output dom nodes :\n" );
	OutputDomNodes( root );
	
	xml2 = DumpDomToXml( root , DUMPOPTIONS_TIGHTENING ) ;
	printf( "Dump dom to xml[%s]\n" , xml2 );
	root2 = ParseXmlToDom( xml2 ) ;
	free( xml2 );
	if( root2 == NULL )
	{
		printf( "*** ERROR : ParseXmlToDom failed[%d]\n" , GetParserLastError() );
		return -1;
	}
	printf( "Output dom nodes :\n" );
	OutputDomNodes( root2 );
	DestroyAllDomNodes( root2 );
	
	xml2 = DumpDomToXml( root , DUMPOPTIONS_INDENTATION ) ;
	printf( "Dump dom to xml[%s]\n" , xml2 );
	root2 = ParseXmlToDom( xml2 ) ;
	free( xml2 );
	if( root2 == NULL )
	{
		printf( "*** ERROR : ParseXmlToDom failed[%d]\n" , GetParserLastError() );
		return -1;
	}
	printf( "Output dom nodes :\n" );
	OutputDomNodes( root2 );
	DestroyAllDomNodes( root2 );
	
	DestroyAllDomNodes( root );
	
	return 0;
}

int main( int argc , char *argv[] )
{
	if( argc != 1 + 1 )
	{
		printf( "USAGE : test_fasterdom4fasterxml xml_file\n" );
		exit(7);
	}
	
	return -test_fasterdom4fasterxml( argv[1] );
}

