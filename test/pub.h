#ifndef _H_PUB_
#define _H_PUB_

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <sys/stat.h>
#if ( defined __unix ) || ( defined __linux__ )
#include <unistd.h>
#endif

#include "fasterdom.h"

void OutputDomNodes( struct DomNode *root_node );

int ReadEntireFileSafely( char *filename , char *mode , char **pbuf , long *pbufsize );

#ifdef __cplusplus
}
#endif

#endif

