#include "fasterdom.h"

#include "pub.h"

int test_basic()
{
	struct DomNode	*root_node = NULL ;
	struct DomNode	*branch_node = NULL ;
	struct DomNode	*array_node = NULL ;
	
	root_node = CreateRootDomNode( NULL , -1 ) ;
	
	branch_node = AddChildDomNode_BRANCH( root_node , "branch_name" , -1 ) ;
	AddChildDomNode_string( branch_node , "str_name" , -1 , "str_data" , -1 );
	AddChildDomNode_long( branch_node , "long_name" , -1 , 123 );
	AddChildDomNode_double( branch_node , "double_name" , -1 , 1.23 );
	AddChildDomNode_bool( branch_node , "bool_name" , -1 , 1 );
	
	array_node = AddChildDomNode_ARRAY( root_node , "array_name" , -1 ) ;
	AddChildDomNode_string( array_node , NULL , -1 , "str_array" , -1 );
	AddChildDomNode_long( array_node , NULL , -1 , 123456 );
	AddChildDomNode_double( array_node , NULL , -1 , 123.456 );
	AddChildDomNode_bool( array_node , NULL , -1 , 0 );
	
	OutputDomNodes( root_node );
	
	DestroyAllDomNodes( root_node );
	
	return 0;
}

int main()
{
	return -test_basic();
}

