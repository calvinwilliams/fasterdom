#include "fasterdom4fasterjson.h"

#include "pub.h"

int test_fasterdom4fasterjson( char *pathfilename )
{
	char		*json = NULL ;
	char		*json2 = NULL ;
	struct DomNode	*root = NULL ;
	struct DomNode	*root2 = NULL ;
	int		nret = 0 ;
	
	nret = ReadEntireFileSafely( pathfilename , "rb" , & json , NULL ) ;
	if( nret )
	{
		printf( "*** ERROR : ReadEntireFileSafely failed\n" );
		return -1;
	}
	printf( "Read file to json[%s]\n" , json );
	
	root = ParseJsonToDom( json ) ;
	free( json );
	if( root == NULL )
	{
		printf( "*** ERROR : ParseJsonToDom failed[%d]\n" , GetParserLastError() );
		return -1;
	}
	printf( "Output dom nodes :\n" );
	OutputDomNodes( root );
	
	json2 = DumpDomToJson( root , 0 ) ;
	printf( "Dump dom to json[%s]\n" , json2 );
	root2 = ParseJsonToDom( json2 ) ;
	free( json2 );
	if( root2 == NULL )
	{
		printf( "*** ERROR : ParseJsonToDom failed[%d]\n" , GetParserLastError() );
		return -1;
	}
	printf( "Output dom nodes :\n" );
	OutputDomNodes( root2 );
	DestroyAllDomNodes( root2 );
	
	json2 = DumpDomToJson( root , DUMPOPTIONS_INDENTATION ) ;
	printf( "Dump dom to json[%s]\n" , json2 );
	root2 = ParseJsonToDom( json2 ) ;
	free( json2 );
	if( root2 == NULL )
	{
		printf( "*** ERROR : ParseJsonToDom failed[%d]\n" , GetParserLastError() );
		return -1;
	}
	printf( "Output dom nodes :\n" );
	OutputDomNodes( root2 );
	DestroyAllDomNodes( root2 );
	
	DestroyAllDomNodes( root );
	
	return 0;
}

int main( int argc , char *argv[] )
{
	if( argc != 1 + 1 )
	{
		printf( "USAGE : test_fasterdom4fasterjson json_file\n" );
		exit(7);
	}
	
	return -test_fasterdom4fasterjson( argv[1] );
}

