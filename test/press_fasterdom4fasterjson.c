#include "fasterdom4fasterjson.h"

#include "pub.h"

int press_fasterdom( char *pathfilename , int count )
{
	char		*json = NULL ;
	struct DomNode	*root_node = NULL ;
	int		i ;
	int		nret = 0 ;
	
	nret = ReadEntireFileSafely( pathfilename , "rb" , & json , NULL ) ;
	if( nret )
	{
		printf( "*** ERROR : ParseJsonToDom failed\n" );
		return -1;
	}
	
	for( i = 0 ; i < count ; i++ )
	{
		root_node = ParseJsonToDom( json ) ;
		if( root_node == NULL )
		{
			printf( "*** ERROR : ParseJsonToDom failed\n" );
			return -1;
		}
		
		DestroyAllDomNodes( root_node );
	}
	
	free( json );
	
	return 0;
}

int main( int argc , char *argv[] )
{
	if( argc != 1 + 2 )
	{
		printf( "USAGE : press_fasterdom json_file count\n" );
		exit(7);
	}
	
	return -press_fasterdom( argv[1] , atoi(argv[2]));
}

