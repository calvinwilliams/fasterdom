#ifndef _H_FASTERDOM_
#define _H_FASTERDOM_

#ifdef __cplusplus
extern "C" {
#endif

#if 0
// #define __USE_QMALLOC
#include "qmalloc2_wrap.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if ( defined _WIN32 )
#include <windows.h>
#ifndef _WINDLL_FUNC
#define _WINDLL_FUNC		_declspec(dllexport)
#endif
#elif ( defined __unix ) || ( defined __linux__ )
#ifndef _WINDLL_FUNC
#define _WINDLL_FUNC		extern
#endif
#endif

#ifndef STRCMP
#define STRCMP(_a_,_C_,_b_) ( strcmp(_a_,_b_) _C_ 0 )
#define STRNCMP(_a_,_C_,_b_,_n_) ( strncmp(_a_,_b_,_n_) _C_ 0 )
#endif

#ifndef STRICMP
#if ( defined _WIN32 )
#define STRICMP(_a_,_C_,_b_) ( stricmp(_a_,_b_) _C_ 0 )
#define STRNICMP(_a_,_C_,_b_,_n_) ( strnicmp(_a_,_b_,_n_) _C_ 0 )
#elif ( defined __unix ) || ( defined __linux__ )
#define STRICMP(_a_,_C_,_b_) ( strcasecmp(_a_,_b_) _C_ 0 )
#define STRNICMP(_a_,_C_,_b_,_n_) ( strncasecmp(_a_,_b_,_n_) _C_ 0 )
#endif
#endif

/*
 * dom
 */

enum DomNodeType
{
	DOMNODE_TYPE_BRANCH = 0 ,
	DOMNODE_TYPE_ARRAY ,
	DOMNODE_TYPE_STRING ,
	DOMNODE_TYPE_LONG ,
	DOMNODE_TYPE_DOUBLE ,
	DOMNODE_TYPE_BOOL
} ;

enum DomNodeExist
{
	DOMNODE_NODE_NOT_EXIST = 0 ,
	DOMNODE_NODE_TYPE_NOT_MATCHED ,
	DOMNODE_NODE_DATA_NOT_EXIST ,
	DOMNODE_NODE_DATA_EXIST
} ;

struct DomNode ;

#define CREATEDOMNODE_AUTOMATIC_LENGTH			-1
#define CREATEDOMNODE_REFERENCE				-2
#define CREATEDOMNODE_REFERENCE_AND_DELEGATION_OF_FREE	-3
_WINDLL_FUNC struct DomNode *CreateDomNode( char *node_name , int node_name_len , enum DomNodeType node_type , void *node_data , int node_data_len );
_WINDLL_FUNC struct DomNode *LinkSiblingDomNode( struct DomNode *prev_node , struct DomNode *node );
_WINDLL_FUNC struct DomNode *LinkChildDomNode( struct DomNode *parent_node , struct DomNode *node );
_WINDLL_FUNC void UnlinkDomNode( struct DomNode *node );
_WINDLL_FUNC void DestroyDomNode( struct DomNode *node );
_WINDLL_FUNC void DestroyAllDomNodes( struct DomNode *nodes );

_WINDLL_FUNC struct DomNode *GetFirstDomNode( struct DomNode *node );
_WINDLL_FUNC struct DomNode *GetLastDomNode( struct DomNode *node );
_WINDLL_FUNC struct DomNode *GetPrevDomNode( struct DomNode *node );
_WINDLL_FUNC struct DomNode *GetNextDomNode( struct DomNode *node );
_WINDLL_FUNC struct DomNode *GetChildrenDomNodes( struct DomNode *node );
_WINDLL_FUNC struct DomNode *GetParentDomNode( struct DomNode *node );

_WINDLL_FUNC char *GetDomNodeName( struct DomNode *node );
_WINDLL_FUNC enum DomNodeType GetDomNodeType( struct DomNode *node );
_WINDLL_FUNC enum DomNodeExist GetDomNodeDataExist( struct DomNode *node );
_WINDLL_FUNC struct DomNode *FindChildDomNode( struct DomNode *parent_node , char *node_name );
_WINDLL_FUNC struct DomNode *FindSiblingDomNode( struct DomNode *node , char *node_name );

_WINDLL_FUNC void SetDomNodeXmlDeclaration( struct DomNode *node , struct DomNode *xml_declaration );
_WINDLL_FUNC struct DomNode *GetDomNodeXmlDeclarationPtr( struct DomNode *node );

_WINDLL_FUNC struct DomNode *CreateRootDomNode( char *node_name , int node_name_len );
_WINDLL_FUNC struct DomNode *AddSiblingDomNode_BRANCH( struct DomNode *prev_node , char *node_name , int node_name_len );
_WINDLL_FUNC struct DomNode *AddSiblingDomNode_ARRAY( struct DomNode *prev_node , char *node_name , int node_name_len );
_WINDLL_FUNC struct DomNode *AddSiblingDomNode_string( struct DomNode *prev_node , char *node_name , int node_name_len , char *s , int len );
_WINDLL_FUNC struct DomNode *AddSiblingDomNode_long( struct DomNode *prev_node , char *node_name , int node_name_len , long l );
_WINDLL_FUNC struct DomNode *AddSiblingDomNode_double( struct DomNode *prev_node , char *node_name , int node_name_len , double d );
_WINDLL_FUNC struct DomNode *AddSiblingDomNode_bool( struct DomNode *prev_node , char *node_name , int node_name_len , unsigned char b );
_WINDLL_FUNC struct DomNode *AddChildDomNode_BRANCH( struct DomNode *parent_node , char *node_name , int node_name_len );
_WINDLL_FUNC struct DomNode *AddChildDomNode_ARRAY( struct DomNode *parent_node , char *node_name , int node_name_len );
_WINDLL_FUNC struct DomNode *AddChildDomNode_string( struct DomNode *parent_node , char *node_name , int node_name_len , char *s , int len );
_WINDLL_FUNC struct DomNode *AddChildDomNode_long( struct DomNode *parent_node , char *node_name , int node_name_len , long l );
_WINDLL_FUNC struct DomNode *AddChildDomNode_double( struct DomNode *parent_node , char *node_name , int node_name_len , double d );
_WINDLL_FUNC struct DomNode *AddChildDomNode_bool( struct DomNode *parent_node , char *node_name , int node_name_len , unsigned char b );

_WINDLL_FUNC char *GetDomNodeData_string( struct DomNode *node , enum DomNodeExist *is_exist );
_WINDLL_FUNC long GetDomNodeData_long( struct DomNode *node , enum DomNodeExist *is_exist );
_WINDLL_FUNC double GetDomNodeData_double( struct DomNode *node , enum DomNodeExist *is_exist );
_WINDLL_FUNC unsigned char GetDomNodeData_bool( struct DomNode *node , enum DomNodeExist *is_exist );

_WINDLL_FUNC char *FindChildDomNodeData_string( struct DomNode *parent_node , char *node_name , enum DomNodeExist *is_exist );
_WINDLL_FUNC long FindChildDomNodeData_long( struct DomNode *parent_node , char *node_name , enum DomNodeExist *is_exist );
_WINDLL_FUNC double FindChildDomNodeData_double( struct DomNode *parent_node , char *node_name , enum DomNodeExist *is_exist );
_WINDLL_FUNC unsigned char FindChildDomNodeData_bool( struct DomNode *parent_node , char *node_name , enum DomNodeExist *is_exist );

_WINDLL_FUNC struct DomNode *LinkXmlPropertyDomNode( struct DomNode *node , struct DomNode *prop_node );

_WINDLL_FUNC struct DomNode *GetFirstXmlPropertyDomNode( struct DomNode *node );
_WINDLL_FUNC struct DomNode *GetNextXmlPropertyDomNode( struct DomNode *node );

_WINDLL_FUNC struct DomNode *FindXmlPropertyDomNode( struct DomNode *node , char *node_name );

_WINDLL_FUNC struct DomNode *AddXmlPropertyDomNode_string( struct DomNode *node , char *prop_name , int prop_name_len , char *s , int len );
_WINDLL_FUNC struct DomNode *AddXmlPropertyDomNode_long( struct DomNode *node , char *prop_name , int prop_name_len , long l );
_WINDLL_FUNC struct DomNode *AddXmlPropertyDomNode_double( struct DomNode *node , char *prop_name , int prop_name_len , double d );
_WINDLL_FUNC struct DomNode *AddXmlPropertyDomNode_bool( struct DomNode *node , char *prop_name , int prop_name_len , unsigned char b );

_WINDLL_FUNC char *FindXmlPropertyDomNode_string( struct DomNode *node , char *prop_name , enum DomNodeExist *is_exist );
_WINDLL_FUNC long FindXmlPropertyDomNode_long( struct DomNode *node , char *prop_name , enum DomNodeExist *is_exist );
_WINDLL_FUNC double FindXmlPropertyDomNode_double( struct DomNode *node , char *prop_name , enum DomNodeExist *is_exist );
_WINDLL_FUNC unsigned char FindXmlPropertyDomNode_bool( struct DomNode *node , char *prop_name , enum DomNodeExist *is_exist );

_WINDLL_FUNC struct DomNode *CreateXmlCdataDomNode( char *node_name , int node_name_len , void *node_data , int node_data_len );
_WINDLL_FUNC unsigned char IsXmlCdataDomNode( struct DomNode *node );

_WINDLL_FUNC void SetParserLastError( int error );
_WINDLL_FUNC int GetParserLastError();

#define DOMNODE_STRINGCACHE_SIZE	16

/*
 * pub
 */

_WINDLL_FUNC int CreateDumpBuffer( char **pp_buf , int *p_buf_size , int *p_buf_len );
_WINDLL_FUNC int ResizeDumpBuffer( char **pp_buf , int *p_buf_size );
_WINDLL_FUNC int AppendDumpBuffer( char **pp_buf , int *p_buf_size , int *p_buf_len , char *format , ... );

#ifdef __cplusplus
}
#endif

#endif

