#ifndef _H_FASTERDOM4FASTERXML_
#define _H_FASTERDOM4FASTERXML_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "fasterdom.h"

#include "fasterxml.h"

_WINDLL_FUNC struct DomNode *ParseXmlToDom( char *json );

#define DUMPOPTIONS_TIGHTENING		0
#define DUMPOPTIONS_INDENTATION		1
_WINDLL_FUNC char *DumpDomToXml( struct DomNode *root , int dump_options );

#ifdef __cplusplus
}
#endif

#endif

